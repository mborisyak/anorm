"""
Adversarial Normalization.
"""

from setuptools import setup, find_packages
import os

here = os.path.dirname(__file__)

with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
  long_description=f.read()

version = '0.3.0'

setup(
  name = 'anorm',
  version=version,
  description="""Adversarial Normalization.""",

  long_description=long_description,
  long_description_content_type="text/markdown",

  url='https://gitlab.com/craynn/craygraph',

  author='Maxim Borisyak and contributors.',
  author_email='maximus.been@gmail.com',

  maintainer='Maxim Borisyak',
  maintainer_email='maximus.been@gmail.com',

  license='MIT',

  classifiers=[
    'Development Status :: 4 - Beta',
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3',
  ],

  packages=find_packages(where='src/', ),
  package_dir={'': 'src/'},

  extras_require={
    'test': [
      'pytest >= 4.0.0',
      'scipy >= 1.4.0'
    ],
  },

  install_requires=[
  ],

  python_requires='>=3.7',
)


