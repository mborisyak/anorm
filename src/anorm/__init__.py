from .plain import Plain
from .l2 import L2
from .l1 import L1
from .dropout import Dropout

from .adv import ChannelAdversarial