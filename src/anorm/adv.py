import tensorflow as tf

from craynn import layers, parameters, updates
from crayopt import tf_updates, train

from .meta import TrainingMethod

__all__ = [
  'ChannelAdversarial'
]

class ChannelAdversarial(TrainingMethod):
  def __init__(
    self,
    model, capacity,
    dataset_train : updates.VariableDataset,
    loss, c_reg: float,
    batch_size: int,
    optimizer=tf_updates.adam(learning_rate=2e-4),
    adversary_steps: int = 4,
    adversary_optimizer=tf_updates.adam(learning_rate=1e-3),
    seed: int=111
  ):
    super(ChannelAdversarial, self).__init__(
      model, capacity,
      dataset_train, loss=loss,
      c_reg=c_reg, batch_size=batch_size,
      optimizer=optimizer
    )

    self.functional_layers = [
      layer
      for layer in self.model.layers()
      if len(parameters.get_parameters(layer)) > 0
    ]

    self.output_shapes = layers.get_all_output_shapes(self.functional_layers)
    self.output_shapes = [
      self.output_shapes[layer]
      for layer in self.functional_layers
    ]

    from craynn import network, conv_1x1, nonlinearities

    self.adversaries = list()
    for shape in self.output_shapes[:-1]:
      adversary = network(shape)(
        conv_1x1(32),
        conv_1x1(1, activation=nonlinearities.linear())
      )
      self.adversaries.append(adversary)

    self.adversary_optimizer = adversary_optimizer([
      var
      for adversary in self.adversaries
      for var in adversary.variables(trainable=True)
    ])

    self.adversary_steps = adversary_steps
    self.rng = tf.random.Generator.from_seed(seed)

  def _get_model(self, model, capacity):
    return model(capacity)

  def _get_step(self):
    @tf.function(autograph=False)
    def step_model():
      X, y = self.dataset_train.batch(self.batch_size)

      with self.optimizer:
        activations = train.get_outputs_without_activation(
          self.functional_layers,
          substitutes=zip(self.model.inputs(), (X,))
        )
        activations, predictions = activations[:-1], activations[-1]

        loss = self.loss_f(y, predictions)
        regularization = sum([
          tf.reduce_mean(tf.nn.softplus(adversary(activation)))
          for adversary, activation in zip(self.adversaries, activations)
        ])
        return self.optimizer(loss - self.c_reg * regularization)

    @tf.function(autograph=False)
    def step_adversaries():
      X, _ = self.dataset_train.batch(self.batch_size)

      activations = train.get_outputs_without_activation(
        self.functional_layers,
        substitutes=zip(self.model.inputs(), (X,))
      )[:-1]

      X_random = [
        self.rng.normal(shape=tf.shape(activation), dtype=activation.dtype)
        for activation in activations
      ]

      with self.adversary_optimizer:
        loss = sum([
          tf.reduce_mean(tf.nn.softplus(-adversary(X_pos))) + tf.reduce_mean(tf.nn.softplus(adversary(X_neg)))
          for adversary, X_pos, X_neg in zip(self.adversaries, X_random, activations)
        ])
        return self.adversary_optimizer(loss)

    @tf.function(autograph=False)
    def step():
      for _ in range(self.adversary_steps):
        step_adversaries()

      step_model()

    return step