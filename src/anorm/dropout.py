import tensorflow as tf
from crayopt import tf_updates

from .meta import TrainingMethod

__all__ = [
  'Dropout'
]

class Dropout(TrainingMethod):
  def __init__(
    self,
    model, capacity,
    dataset_train,
    loss, c_reg: float, p_dropout: float,
    batch_size: int,
    optimizer=tf_updates.adam(learning_rate=2e-4),
  ):
    self.p_dropout = p_dropout

    super(Dropout, self).__init__(
      model, capacity,
      dataset_train, loss=loss, c_reg=c_reg, batch_size=batch_size,
      optimizer=optimizer
    )

  def _get_model(self, model, capacity):
    from craynn import conv, dropout

    return model(
      capacity,
      conv=lambda *args, **kwargs: (
        conv(*args, **kwargs),
        dropout(p=self.p_dropout)
      )
    )


  def _get_step(self):
    @tf.function(autograph=False)
    def step():
      X, y = self.dataset_train.batch(self.batch_size)

      with self.optimizer:
        predictions = self.model(X)
        loss = self.loss_f(y, predictions)
        reg = self.model.reg_l2()
        return self.optimizer(loss + self.c_reg * reg)

    return step