from crayopt import train, tf_updates

__all__ = [
  'TrainingMethod'
]

class TrainingMethod(object):
  def __init__(
    self,
    model, capacity,
    dataset_train,
    loss, c_reg: float, batch_size: int,
    optimizer=tf_updates.adam(learning_rate=2e-4)
  ):
    self.dataset_train = dataset_train

    self.loss_f = loss
    self.c_reg = c_reg
    self.batch_size = batch_size

    self.model = self._get_model(model, capacity)
    self.optimizer = optimizer(self.model.variables(trainable=True))

    self._step = self._get_step()

  def name(self):
    return self.__class__.__name__

  def _get_model(self, model, capacity):
    return model(capacity)

  def _get_step(self):
    raise NotImplementedError()

  def train(self, n_epoches, progress=None):
    if n_epoches < 1:
      import numpy as np
      return np.zeros(shape=(0, 0), dtype='float32')

    self.model.reset()
    X_input, _ = self.dataset_train[:self.batch_size]
    train.normalize_weights(self.model, inputs=(X_input, ), progress=progress)
    result = train.iterate(self._step, len(self.dataset_train) // self.batch_size, n_epoches, progress=progress)

    return result