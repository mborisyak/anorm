import tensorflow as tf

from .meta import TrainingMethod

__all__ = [
  'Plain'
]

class Plain(TrainingMethod):
  def _get_step(self):
    @tf.function(autograph=False)
    def step():
      X, y = self.dataset_train.batch(self.batch_size)

      with self.optimizer:
        predictions = self.model(X)
        loss = self.loss_f(y, predictions)
        reg = self.model.reg_l2()
        return self.optimizer(loss + self.c_reg * reg)

    return step