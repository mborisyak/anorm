from craynn import network, conv, max_pool, dense, flatten, elu, linear

__all__ = [
  'logreg'
]

def logreg(n, conv=conv, max_pool=max_pool, dense=dense):
  return network((None, 1, 28, 28))(
    flatten(),
    dense(10, activation=linear())
  )