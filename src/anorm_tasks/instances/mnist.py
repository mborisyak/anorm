from anorm_tasks.meta import Task

class MNIST(Task):
  @classmethod
  def flow(cls, root=None):
    import crayflow as flow

    return flow.dataflow(
      flow.instances.download_mnist('MNIST/'),
      flow.stage(name='read')(
        flow.instances.read_mnist()
      ) @ flow.pickled('MNIST/MNIST.pickled')
    )(root=root)


  def __init__(self, seed, root):
    from craynn import variable_dataset
    from craynn import objectives

    super(MNIST, self).__init__(seed, root)

    data_train, labels_train, data_test, labels_test = self.flow(root=root)

    self._dataset_train = variable_dataset(data_train, labels_train, seed=seed)
    self._dataset_test = variable_dataset(data_test, labels_test, seed=seed)

    self._loss_f = objectives.logit_categorical_crossentropy()

  def loss_f(self):
    return self._loss_f

  def dataset_train(self):
    return self._dataset_train

  def dataset_test(self):
    return self._dataset_test

  def models(self):
    from ..conv_models import VGG_28x28
    from ..logreg import logreg
    return dict(
      VGG=VGG_28x28,
      logreg=logreg
    )

  def eval(self, net):
    from ..utils import eval_classification

    return eval_classification(
      self._dataset_train,
      self._dataset_test,
      net,
      batch_size=32,
    )


