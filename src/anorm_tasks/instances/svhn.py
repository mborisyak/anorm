from anorm_tasks.meta import Task

class SVHN(Task):
  @classmethod
  def flow(cls, root=None):
    import crayflow as flow

    return flow.dataflow(
      flow.instances.download_svhn_cropped('SVHN/'),
      flow.stage(name='read')(
        flow.instances.read_svhn_cropped()
      ) @ flow.pickled('SVHN/SVHN.pickled')
    )(root=root)


  def __init__(self, seed, root):
    import crayflow as flow

    from craynn import variable_dataset
    from craynn import objectives

    super(SVHN, self).__init__(seed, root)

    data_train, labels_train, data_test, labels_test = self.flow(root=root)
    data_train = data_train.astype('float32') / 255.0
    data_test = data_test.astype('float32') / 255.0

    labels_train = flow.data.onehot(10)(labels_train)
    labels_test = flow.data.onehot(10)(labels_test)

    self._dataset_train = variable_dataset(data_train, labels_train, seed=seed)
    self._dataset_test = variable_dataset(data_test, labels_test, seed=seed)

    self._loss_f = objectives.logit_categorical_crossentropy()

  def loss_f(self):
    return self._loss_f

  def dataset_train(self):
    return self._dataset_train

  def dataset_test(self):
    return self._dataset_test

  def models(self):
    from ..conv_models import VGG_32x32

    return dict(
      VGG=VGG_32x32
    )

  def eval(self, net):
    from ..utils import eval_classification

    return eval_classification(
      self._dataset_train,
      self._dataset_test,
      net,
      batch_size=32,
    )


