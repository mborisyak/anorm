from anorm_tasks.meta import Task

class CIFAR(Task):
  @classmethod
  def flow(cls, root=None):
    import crayflow as flow

    return flow.dataflow(
      flow.instances.download_cifar10('CIFAR/'),
      flow.stage(name='read')(
        flow.instances.read_cifar10()
      ) @ flow.pickled('CIFAR/CIFAR-10.pickled')
    )(root=root)


  def __init__(self, seed, root):
    from craynn import variable_dataset
    from craynn import objectives

    super(CIFAR, self).__init__(seed, root)

    data_train, labels_train, data_test, labels_test = self.flow(root=root)

    self._dataset_train = variable_dataset(data_train, labels_train, seed=seed)
    self._dataset_test = variable_dataset(data_test, labels_test, seed=seed)

    self._loss_f = objectives.logit_categorical_crossentropy()

  def loss_f(self):
    return self._loss_f

  def dataset_train(self):
    return self._dataset_train

  def dataset_test(self):
    return self._dataset_test

  def model(self, model_name='VGG'):
    models = self.models()

    if model_name in models:
      return models[model_name]
    else:
      raise Exception('Model %s is not defined for MNIST dataset' % (model_name, ))

  def models(self):
    from ..conv_models import VGG_32x32

    return dict(
      VGG=VGG_32x32
    )

  def eval(self, net):
    from ..utils import eval_classification

    return eval_classification(
      self._dataset_train,
      self._dataset_test,
      net,
      batch_size=32,
    )


