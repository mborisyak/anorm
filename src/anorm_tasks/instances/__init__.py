from .mnist import MNIST
from .cifar import CIFAR
from .svhn import SVHN