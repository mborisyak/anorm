from craynn import network, conv, max_pool, dense, flatten, elu, linear

__all__ = [
  'VGG_28x28', 'VGG_32x32'
]

def VGG_32x32(n, conv=conv, max_pool=max_pool, dense=dense):
  return network((None, 3, 32, 32))(
    conv(2 * n, activation=elu()),
    conv(2 * n, activation=elu()),
    max_pool(),

    conv(3 * n, activation=elu()),
    conv(3 * n, activation=elu()),
    max_pool(),

    conv(4 * n, activation=elu()),
    conv(4 * n, activation=elu()),

    flatten(),
    dense(10, activation=linear())
  )

def VGG_28x28(n, conv=conv, max_pool=max_pool, dense=dense):
  return network((None, 1, 28, 28))(
    conv(2 * n, activation=elu()),
    conv(2 * n, activation=elu()),
    max_pool(),

    conv(3 * n, activation=elu()),
    conv(3 * n, activation=elu()),
    max_pool(),

    conv(4 * n, activation=elu()),
    max_pool(),

    flatten(),
    dense(10, activation=linear())
  )