__all__ = [
  'Task'
]

class Task(object):
  @classmethod
  def flow(cls, root=None):
    raise NotImplementedError()

  def __init__(self, seed, root):
    self._seed = seed
    self._root = root

  def name(self):
    return self.__class__.__name__

  def dataset_train(self):
    raise NotImplementedError()

  def dataset_test(self):
    raise NotImplementedError()

  def eval(self, model):
    raise NotImplementedError()

  def model(self, model_name):
    models = self.models()

    if model_name in models:
      return models[model_name]
    else:
      raise Exception('Model %s is not defined for %s dataset' % (model_name, self.name()))

  def models(self):
    raise NotImplementedError()

  def loss_f(self):
    raise NotImplementedError()