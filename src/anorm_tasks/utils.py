__all__ = [
  'eval_classification'
]

def _eval_classification(dataset, net, batch_size):
  import numpy as np
  import tensorflow as tf
  from craynn import objectives

  X, y = dataset.variables()

  loss_f = objectives.logit_categorical_crossentropy()
  predictions, = dataset.map(net.mode(deterministic=True)).data(batch_size=batch_size)

  loss = tf.reduce_mean(loss_f(y, predictions)).numpy()
  accuracy = np.mean(
    np.argmax(predictions, axis=1) == np.argmax(y.numpy(), axis=1)
  )

  return loss, accuracy

def eval_classification(dataset_train, dataset_test, net, batch_size):
  loss_train, accuracy_train = _eval_classification(dataset_train, net, batch_size=batch_size)
  loss_test, accuracy_test = _eval_classification(dataset_test, net, batch_size=batch_size)

  return dict(
    train=dict(
      accuracy=float(accuracy_train),
      loss=float(loss_train),
    ),

    test=dict(
      accuracy=float(accuracy_test),
      loss=float(loss_test),
    )
  )