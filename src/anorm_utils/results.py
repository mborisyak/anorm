import os

import numpy as np

import json
import pickle

__all__ = [
  'aggregate_results',
  'combine',
  'sort_by'
]

def combine(results):
  metrics = set(
    metric
    for result in results
    for metric in result['train']
  )

  combined = {
    metric : dict(train=list(), test=list())
    for metric in metrics
  }

  for result in results:
    for metric in metrics:
      combined[metric]['train'].append(result['train'][metric])
      combined[metric]['test'].append(result['test'][metric])

  return {
    metric : dict(
      train=np.array(combined[metric]['train'], dtype='float32'),
      test=np.array(combined[metric]['test'], dtype='float32')
    )
    for metric in combined
  }

def aggregate_results(root, *keys):
  if len(keys) == 0:
    return dict()

  results = dict()

  paths = [
    os.path.join(root, item)
    for item in os.listdir(root)
    if item.endswith('.json')
  ]

  for path in paths:
    with open(path, 'r') as f:
      result = json.load(f)

    results_ = results

    for key in keys:
      if key not in result['args']:
        print(result['args'])
        raise Exception('%s is not present in %s' % (key, path))

      k = result['args'][key]

      if k not in results_:
        results_[k] = dict()

      results_ = results_[k]

    rest = tuple(
      (k, v)
      for k, v in result['args'].items()
      if k not in keys
    )

    results_[rest] = combine(result['results'])

  return results

def sort_by(results, key):
  metrics = set(
    metric
    for args in results
    for metric in results[args]
  )

  variables =list()
  output = dict()

  for metric in metrics:
    output[metric] = dict(
      train=list(),
      test=list()
    )

  for args, res in results.items():
    args = dict(args)
    value = args[key]

    variables.append(float(value))

    for metric in res:
      output[metric]['train'].append(res[metric]['train'])
      output[metric]['test'].append(res[metric]['test'])

  variables = np.array(variables, dtype=float)
  indx = np.argsort(variables)

  for metric in metrics:
    for k in ('train', 'test'):
      output[metric][k] = [output[metric][k][i] for i in indx]

  return variables[indx], output