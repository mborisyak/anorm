#!/bin/bash

REGS="1e-2 2e-2 5e-2 1e-1 2e-1 5e-1 1"

TASK=$1
REPEAT=$2

python scripts/warmup.py task="$TASK"

mkdir -p logs/

for C in $REGS
do
  sbatch --gpus=1 -c 1 --job-name="CAdv-$C" --error="logs/$TASK-CAdv-$C.err" --output="logs/$TASK-CAdv-$C.out" scripts/main.sh \
    task="$TASK" model=VGG capacity=16 n_epoches=64 method=ChannelAdversarial progress=none \
    training.c_reg="$C" repeat="$REPEAT" &
done

wait