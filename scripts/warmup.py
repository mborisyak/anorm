import os
import gearup

def main():
  import anorm_tasks

  def warmup(
    task: gearup.member[anorm_tasks],
    root: str = os.environ.get('DATA_ROOT', '.')
  ):
    _ = task.flow(root)

  gearup.gearup(warmup)()

if __name__ == '__main__':
  main()