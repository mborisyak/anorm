import os
import json
import pickle

from tqdm import tqdm
import gearup

import numpy as np
import tensorflow as tf
gpu = tf.config.get_visible_devices('GPU')[0]
tf.config.experimental.set_memory_growth(gpu, enable=True)

import anorm
import anorm_tasks

def get_filename(args):
  import hashlib

  h = hashlib.sha256()
  h.update(pickle.dumps(args))

  return h.hexdigest()

def main(
  task: gearup.member[anorm_tasks],
  model: str,
  capacity: int,
  method: gearup.member[anorm],
  root: str = os.environ.get('DATA_ROOT', '.'),
  batch_size: int = 32,
  n_epoches: int = 32,
  repeat: int = 1,
  progress: gearup.choice(none=None, tqdm=tqdm) = tqdm,
  result_dir: str ='results/',
  seed: int = 11111,
  **kwargs
):
  os.makedirs(result_dir, exist_ok=True)

  args = dict(
    task=task.__name__,
    model=model,
    capacity=capacity,
    method=method.__name__,
    batch_size=batch_size,
    n_epoches=n_epoches,
    seed=seed,
    **kwargs.get('training', dict())
  )

  filename = get_filename(args)
  results_path = os.path.join(result_dir, '%s.json' % (filename,))
  train_data_path = os.path.join(result_dir, '%s.pickled' % (filename,))

  try:
    with open(results_path, 'r') as f:
      results = json.load(f)
    assert pickle.dumps(results['args']) == pickle.dumps(args)

  except (FileNotFoundError, IOError):
    results = dict(
      args=args,
      results=list()
    )

  try:
    with open(train_data_path, 'rb') as f:
      train_data = pickle.load(f)

  except (FileNotFoundError, IOError):
    train_data = dict(
      parameters=list(),
      losses=list(),
    )

  assert len(results['results']) == len(train_data['parameters'])
  assert len(results['results']) == len(train_data['losses'])

  rng = np.random.RandomState(seed=seed)
  seeds = tuple(
    int(rng.randint(0, 2 ** 63 - 1, dtype='int64'))
    for _ in range(repeat)
  )

  for i in range(repeat):
    if i < len(train_data['parameters']):
      continue

    dataset = task(seeds[i], root)
    algorithm = gearup.apply(
      method,
      model=dataset.model(model),
      capacity=capacity,
      dataset_train=dataset.dataset_train(),
      loss=dataset.loss_f(),
      batch_size=batch_size,
      **kwargs.get('training', dict())
    )

    losses = algorithm.train(n_epoches, progress=progress)
    result = dataset.eval(algorithm.model)

    results['results'].append(result)
    train_data['parameters'].append([
      var.numpy()
      for var in algorithm.model.variables()
    ])
    train_data['losses'].append(losses)

  with open(results_path, 'w') as f:
    json.dump(results, f, indent=2)

  with open(train_data_path, 'wb') as f:
    pickle.dump(train_data, f)

if __name__ == '__main__':
  gearup.gearup(main)()