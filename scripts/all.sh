#!/bin/bash

if [ -z "$1" ]
then
  REPEAT=10
else
  REPEAT="$1"
fi

bash scripts/capacities.sh MNIST "$REPEAT"
bash scripts/capacities.sh CIFAR "$REPEAT"
bash scripts/capacities.sh SVHN "$REPEAT"

bash scripts/l2.sh MNIST "$REPEAT"
bash scripts/l2.sh CIFAR "$REPEAT"
bash scripts/l2.sh SVHN "$REPEAT"

bash scripts/l1.sh MNIST "$REPEAT"
bash scripts/l1.sh CIFAR "$REPEAT"
bash scripts/l1.sh SVHN "$REPEAT"

bash scripts/dropout.sh MNIST "$REPEAT"
bash scripts/dropout.sh CIFAR "$REPEAT"
bash scripts/dropout.sh SVHN "$REPEAT"