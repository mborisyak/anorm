#!/bin/bash

REGS="1e-3 2e-3 3e-3 5e-3 7e-3 1e-2"

TASK=$1
REPEAT=$2

python scripts/warmup.py task="$TASK"

mkdir -p logs/

for C in $REGS
do
  sbatch --gpus=1 -c 1 --job-name="L2-$C" --error="logs/$TASK-L2-$C.err" --output="logs/$TASK-L2-$C.out" scripts/main.sh \
    task="$TASK" model=VGG capacity=16 n_epoches=64 method=L2 progress=none \
    training.c_reg="$C" repeat="$REPEAT" &
done

wait