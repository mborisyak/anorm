#!/bin/bash

PS="0.025 0.05 0.075 0.1 0.125 0.15 0.2 0.25"

TASK=$1
REPEAT=$2

python scripts/warmup.py task="$TASK"

mkdir -p logs/

for P in $PS
do
  sbatch --gpus=1 -c 1 --job-name="Drop-$P" --error="logs/$TASK-Drop-$P.err" --output="logs/$TASK-Drop-$P.out" scripts/main.sh \
    task="$TASK" model=VGG capacity=16 n_epoches=64 method=Dropout progress=none \
    training.c_reg=1e-3 training.p_dropout="$P" repeat="$REPEAT" &
done

wait