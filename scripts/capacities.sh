#!/bin/bash

CAPACITIES="4 6 8 12 16 24 32 48 64"

TASK=$1
REPEAT=$2

python scripts/warmup.py task="$TASK"

mkdir -p logs/

for N in $CAPACITIES
do
  sbatch --gpus=1 -c 1 --job-name="CAP-$N" --error="logs/$TASK-CAP-$N.err" --output="logs/$TASK-CAP-$N.out" scripts/main.sh \
    task="$TASK" model=VGG capacity="$N" n_epoches=64 method=Plain progress=none \
    training.c_reg=1e-3 repeat="$REPEAT" &
done

wait